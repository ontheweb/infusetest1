1. Make git clone

```sh
cd <project_folder>
git clone https://gitlab.com/ontheweb/infusetest1
```

2. Run docker containers

```sh
docker-compose up -d
```

3. Go to the test-app container and run /bin/bash. Next make composer install

```sh
docker-compose exec -it test-app /bin/bash
composer install
```

4. Now you can see the project in your browser by link http://localhost:9000/ or http://localhost:9001/ for PHPMyAdmin

Project includes some classes with composer autoload and additions for future