<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require __DIR__ . '/vendor/autoload.php';

use Test\App\Database\MySQL;
use Test\App\Database\DatabaseService;
use Test\App\DotEnv;
use Test\App\Visitor;
use Test\App\Image;

// Use .env file to set database connection parameters
DotEnv::load('.env');

/* You could make any DB types connection
 * by creating your own class and implements interface DatabaseInterface
 */
$db = new MySQL(
    getenv('DB_HOST'),
    getenv('DB_DATABASE'),
    getenv('DB_USERNAME'),
    getenv('DB_PASSWORD')
);

// Makes universal polymorph object for working with DB
$dbConnection = new DatabaseService();
$dbConnection->connect($db);
$dbConnection->createViewsTableIfNotExists($db);

$visitor = new Visitor();
$visitor->rememberVisitor($dbConnection, $db);


$banner = new Image(__DIR__ . '/images/memes');

$banner->show();

$dbConnection->close($db);