<?php

namespace Test\App;

use ErrorException;
     
class DotEnv {

    public static function load($path = "")
    {

        $tmpEnv;

        // Check if .env file path has provided
        if(empty($path)){
            throw new ErrorException(".env file path is missing");
        }

        //Check .envenvironment file exists
        if(!is_file(realpath($path))){
            throw new ErrorException("Environment File is Missing");
        }

        //Check .envenvironment file is readable
        if(!is_readable(realpath($path))){
            throw new ErrorException("Permission Denied for reading the " . (realpath($path)));
        }
        
        $tmpEnv = [];
        $fopen = fopen(realpath($path), 'r');

        if($fopen){
            while (($line = fgets($fopen)) !== false){
                
                // Check if line is a comment
                $lineIsComment = (substr(trim($line),0 , 1) == '#') ? true : false;
                if($lineIsComment || empty(trim($line)))
                    continue;

                $lineNoComment = explode("#", $line, 2)[0];
                $envEx = preg_split('/(\s?)\=(\s?)/', $lineNoComment);
                $envName = trim($envEx[0]);
                $envValue = isset($envEx[1]) ? trim($envEx[1]) : "";
                $tmpEnv[$envName] = $envValue;

            }
            fclose($fopen);
        }

        // Save .env data to Environment Variables
        foreach($tmpEnv as $name=>$value){

            putenv("{$name}=$value");

            if(is_numeric($value))
            $value = floatval($value);

            if(in_array(strtolower($value), ["true", "false"]))
            $value = (strtolower($value) == "true") ? true : false;
            $_ENV[$name] = $value;

        }
        
    }
}
