<?php

namespace Test\App\Database;

use PDO;

interface DatabaseInterface {

    /**
     * Return the DB connection
     * 
     * @return mixed
     */
    public function connect();

    /**
     * Create special Data table if not exists
     * If we need wide functionality in the future,
     * we should make a fully QueryBuilder
     * 
     * @return void
     */
    public function createViewsTableIfNotExists();

    /**
     * Сhecking the presence of a visitor in the views table
     * Return record ID or TRUE
     * 
     * @return bool|int
     */
    // public function isVisitorUnique(): bool|int ;

    public function recordVisitorToDB(string $ip, string $agent, string $url);

    /**
     * Close the DB connection
     */
    public function close();

}