<?php

namespace Test\App\Database;

use PDO;
use ErrorException;

class MySQL implements DatabaseInterface {

    private $connection;

    public function __construct(
        private string $hostName,
        private string $dbName,
        private string $userName,
        private string $password
        )
    {}

    public function connect()
    {
        try {

            $this->connection = new PDO("mysql:host=$this->hostName;dbname=$this->dbName", $this->userName, $this->password);

            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // return $connection;

        } catch(PDOException $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    public function createViewsTableIfNotExists()
    {

        // SQL query with unique constraint to use the same ip_address, user_agent, page_url
        $sql = "CREATE TABLE IF NOT EXISTS `views` (
            id BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            ip_address VARCHAR(255) NOT NULL,
            user_agent VARCHAR(255) NOT NULL,
            view_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            page_url VARCHAR(255),
            views_count INT(11) NOT NULL DEFAULT(1),
            UNIQUE (ip_address, user_agent, page_url)
        )";

        // we use exec() because no results are returned
        $this->connection->exec($sql);

    }

    /**
     * Сhecking the presence of a visitor in the views table
     * Return record ID or TRUE
     * 
     * @return bool|int
     */
    private function isVisitorUnique($ip, $agent, $url): bool|int 
    {
        $sql = "SELECT id FROM views WHERE ip_address='$ip' AND user_agent='$agent' AND page_url='$url' LIMIT 1";

        $queryResult = $this->connection->query($sql)->fetch();

        if(!empty($queryResult['id'])) {
            return $queryResult['id'];
        } else {
            return true;
        }
    }

    public function recordVisitorToDB(string $ip, string $agent, string $url): void
    {
        $isVisitor = $this->isVisitorUnique($ip, $agent, $url);

        // Create different SQL query string depending on the availability of the record
        if( $isVisitor === true) {
            $sql = "INSERT INTO views SET ip_address='$ip', user_agent='$agent', page_url='$url'";
        } else if (!empty($isVisitor)) {
            if(gettype($isVisitor) === "integer")
            $sql = "UPDATE `views` SET `views_count` = `views_count` + '1' WHERE `views`.`id` = $isVisitor";
        }

        $this->connection->exec($sql);

    }

    public function close(): void
    {
        $this->connection = null;
    }
}