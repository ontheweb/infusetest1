<?php

namespace Test\App\Database;

use Test\App\Database\DatabaseInterface;

class DatabaseService {

    public function connect (DatabaseInterface $databaseSystem)
    {
        $databaseSystem->connect();
    }

    public function createViewsTableIfNotExists(DatabaseInterface $databaseSystem)
    {
        $databaseSystem->createViewsTableIfNotExists();
    }

    public function recordVisitorToDB(DatabaseInterface $databaseSystem, $ip, $agent, $url)
    {
        $databaseSystem->recordVisitorToDB($ip, $agent, $url);
    }

    public function close (DatabaseInterface $databaseSystem)
    {
        $databaseSystem->close();
    }

}