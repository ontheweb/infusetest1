<?php

namespace Test\App;

use ErrorException;

class Image {

    public function __construct(private string $dir){}

    private function getRandomImgPath()
    {
        
        $files = scandir($this->dir);
            
        if (($key = array_search('.', $files)) !== false) {
            unset($files[$key]);
        }
            
        if (($key = array_search('..', $files)) !== false) {
            unset($files[$key]);
        }
            
        $file = array_rand($files);
        
        return $files[$file];
    }

    private function getMimeType($imgPath): string
    {

        return mime_content_type($imgPath);
    }
    

    public function show()
    {
        $imgPath = $this->dir . '/' . $this->getRandomImgPath();
        $imgMimeType = $this->getMimeType($imgPath);

        if ($imgMimeType !== "image/png" && $imgMimeType !== "image/jpeg") {
            throw new ErrorException("File is not jpeg or png image");
        }

        header('Content-Type: ' . $imgMimeType);

        if ($imgMimeType === "image/png") {
            $img = imagecreatefrompng($imgPath);
            imagepng($img);
        } else {
            $img = @imagecreatefromjpeg($imgPath);
            imagejpeg($img);
        }
        
        imagedestroy($img);
    }

}