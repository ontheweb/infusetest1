<?php

namespace Test\App;

use Test\App\Database\DatabaseService;
use Test\App\Database\MySQL;

class Visitor {

    private string $ip, $agent, $url;

    public function __construct()
    {
        /* Make visitor array for future working with it
         * For example, we should make data validating for security
        */ 
        $visitor = [
            'ip' => $_SERVER['REMOTE_ADDR'],
            'agent' => $_SERVER['HTTP_USER_AGENT'],
            'url' => $_SERVER['HTTP_REFERER']
        ];

        $this->ip = $visitor['ip'];
        $this->agent = $visitor['agent'];
        $this->url = empty($visitor['url']) ? "" : $visitor['url'];

    }

    public function rememberVisitor(DatabaseService $dbConnection, MySQL $db)
    {
        $dbConnection->recordVisitorToDB($db, $this->ip, $this->agent, $this->url);
    }

}